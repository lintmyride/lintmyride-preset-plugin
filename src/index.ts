import { AvailablePresets, PresetExports } from "lintmyride";

const pluginPreset: PresetExports = {
  presetId: "lintmyride-plugin",
  preset: {
    extends: ["js/recommended"],
    rules: {
      "js/lockfile": "error",
      "js/nvm": "error",
      "js/side-effects": "error",
    },
  },
};

export const presets: AvailablePresets = [
  pluginPreset,
];
