# Changelog

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.0] - 2020-08-08

### New features

First release of a preset for official Lint My Ride plugins.
Enables the JS plugin and turns some of its rules into errors.
